Демо-пример каталога
====================

Установка
------

```
git clone https://range-r@bitbucket.org/range-r/scena-test.git
cd project
composer global require "fxp/composer-asset-plugin"
composer install
```

Добавьте файл с настройками базы данных `config/db.php`:

```php
<?php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=yii2basic',
    'username' => 'root',
    'password' => '1234',
    'charset' => 'utf8',
];
```

Выполните миграции:

```
php yii migrate
```

Загрузите демонстрационные данные:

```
php yii fixture/load '*' --namespace='app\fixtures'
```

Настроить Apache
```
DocumentRoot "path/to/basic/web"

<Directory "path/to/basic/web">
    RewriteEngine on

    # Если запрашиваемая в URL директория или файл существуют обращаемся к ним напрямую
    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteCond %{REQUEST_FILENAME} !-d
    # Если нет - перенаправляем запрос на index.php
    RewriteRule . index.php

    # ...прочие настройки...
</Directory>
```

Создать index.php в папке web, если он не был создан до этого
```
use yii\web\Application;

if($_SERVER['REMOTE_ADDR'] == '127.0.0.1') {
    defined('YII_DEBUG') or define('YII_DEBUG', true);
    defined('YII_ENV') or define('YII_ENV', 'dev');
}

require(__DIR__ . '/../vendor/autoload.php');
require(__DIR__ . '/../vendor/yiisoft/yii2/Yii.php');


$config = require(__DIR__ . '/../config/web.php');

(new Application($config))->run();
```
